package com.example.shankar.jsouptest1;



        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v7.app.ActionBarActivity;

        import android.view.View;
        import android.widget.Button;
        import android.widget.TextView;

        import org.jsoup.Connection;
        import org.jsoup.Jsoup;
        import org.jsoup.nodes.Document;

        import java.io.IOException;
        import java.util.Map;


public class MainActivity extends ActionBarActivity {

    private Document htmlDocument;
    private String htmlPageUrl = "https://www.rajagiritech.ac.in/stud/parent/";
    private TextView parsedHtmlNode;
    private String htmlContentInStringFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parsedHtmlNode = (TextView)findViewById(R.id.textView);
        Button htmlTitleButton = (Button)findViewById(R.id.button);
        htmlTitleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsoupAsyncTask jsoupAsyncTask = new JsoupAsyncTask();
                jsoupAsyncTask.execute();
            }
        });
    }


    private class JsoupAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {


                Connection.Response res = Jsoup.connect("https://www.rajagiritech.ac.in/stud/parent/varify.asp?action=login")
                        .data("user", "U1504045")
                        .data("pass", "15253")
                        .followRedirects(true)
                        .method(Connection.Method.POST)
                        .execute();

               //Document doc2 = res.parse();
                Document doc2 = Jsoup.connect("https://www.rajagiritech.ac.in/stud/KTU/Parent/Leave.asp?code=2017S4IT")
                        .cookies(res.cookies())
                        .get();


                htmlContentInStringFormat = doc2.text();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            parsedHtmlNode.setText(htmlContentInStringFormat);
        }
    }
}